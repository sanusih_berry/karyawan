/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.user;


import com.daksa.karyawan.entity.Ability;
import com.daksa.karyawan.entity.Role;
import com.daksa.karyawan.repository.AbilityRepository;
import com.daksa.karyawan.service.RoleService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class CreateRoleBacking implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(CreateRoleBacking.class);
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private AbilityRepository abilityRepository;
	@Inject
	private RoleService roleService;
	
	private Role newRole;
	private List<Ability> listAbility;
	private List<Ability> selectedAbility;
	
	public void init() {
		newRole = new Role();
		listAbility = abilityRepository.findAll();
	}
	
	public void createRole() {
		try {
			newRole.setId(UUID.randomUUID().toString().replace("-", ""));			
			if (!selectedAbility.isEmpty()) {
				newRole.setRoleAbility(selectedAbility);			
			}			
			roleService.create(newRole);
			logger.info("Register new role : " + newRole.getName());
			newRole = new Role();
			selectedAbility = new ArrayList<>();
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "Success",
					"Role already created"));						
		} catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error",
					"Failed to create role"));
			logger.error("Error Create Role "+ e.getMessage() , e);
		}
	}
	
	public Role getNewRole() {
		return newRole;
	}

	public void setNewRole(Role newRole) {
		this.newRole = newRole;
	}

	public List<Ability> getListAbility() {
		return listAbility;
	}

	public void setListAbility(List<Ability> listAbility) {
		this.listAbility = listAbility;
	}

	public List<Ability> getSelectedAbility() {
		return selectedAbility;
	}

	public void setSelectedAbility(List<Ability> selectedAbility) {
		this.selectedAbility = selectedAbility;
	}
}
