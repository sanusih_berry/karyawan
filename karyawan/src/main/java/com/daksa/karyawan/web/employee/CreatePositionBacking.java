/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.employee;

import com.daksa.karyawan.exception.CreatePositionException;
import com.daksa.karyawan.service.CreatePositionService;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class CreatePositionBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(CreatePositionBacking.class);
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private CreatePositionService createPositionService;
	
	private String positionName;
	private BigDecimal basicSalary;

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public BigDecimal getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(BigDecimal basicSalary) {
		this.basicSalary = basicSalary;
	}
	
	public void createPosition() {
		try {
			createPositionService.createPosition(positionName, basicSalary);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Position : " + positionName + " created"));	
		} 
		catch (CreatePositionException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));	
		}
	}
}
