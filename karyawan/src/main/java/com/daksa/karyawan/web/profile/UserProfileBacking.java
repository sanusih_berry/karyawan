/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.profile;


import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.security.UserInfo;
import com.daksa.karyawan.infrastructure.security.UserSession;
import com.daksa.karyawan.repository.UserRepository;
import com.daksa.karyawan.service.UserService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class UserProfileBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(UserProfileBacking.class);
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private UserRepository userRepository;
	@Inject
	private UserService userService;
	@Inject
	private UserSession userSession;		
	
	private String userId;
	private String fullName;
	private String email;
	private String username;
	private String role;
	private UserInfo currentUserInfo;
	private User currentUser;
	
	public void init() {
		currentUserInfo = userSession.getUserInfo();
		currentUser = userRepository.find(currentUserInfo.getUserId());

		userId = currentUser.getId();
		username = currentUser.getUsername();
		email = currentUser.getEmail();
		fullName = currentUser.getFullName();		
	}
	
	public void updateProfile() {
		try {
			currentUser.setFullName(fullName);
			currentUser.setEmail(email);			
			userService.edit(currentUser);
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "Success",
					"Profile already updated"));
			logger.info("Update Profile");
		} catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error",
					"Failed to create user"));			
			logger.error(e.getMessage(), e);
		}
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public UserInfo getCurrentUserInfo() {
		return currentUserInfo;
	}

	public void setCurrentUserInfo(UserInfo currentUserInfo) {
		this.currentUserInfo = currentUserInfo;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
	
}
