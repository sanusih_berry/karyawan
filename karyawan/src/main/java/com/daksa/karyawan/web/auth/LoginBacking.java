/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.auth;

import com.daksa.karyawan.entity.Ability;
import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.security.UserInfo;
import com.daksa.karyawan.infrastructure.security.UserSession;
import com.daksa.karyawan.repository.UserRepository;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class LoginBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(LoginBacking.class);		
	
	private String username;
	private String password;
	private boolean showCaptcha;
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private UserRepository userRepository;
	@Inject
	private UserSession userSession;	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	private String getIpByRequest(HttpServletRequest request) {
		String rvalue = request.getRemoteAddr();
		
		String xForwardedFor = request.getHeader("X-Forwarded-For");
		if(xForwardedFor != null && !xForwardedFor.isEmpty()) {
			String[] detail = xForwardedFor.split(",");
			if(detail[0] != null && !detail[0].isEmpty()) {
				rvalue = detail[0];
			}
		}
		
		return rvalue;
	}
	
	public void login() {
		logger.info("Login");
		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		String ip = getIpByRequest(request);
		String returnUrl = (String) facesContext.getExternalContext().getSessionMap().get("returnUrl");
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);		
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {			
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Already Login", "Please logout first!"));
		}
		else {
			subject.getSession().stop();
			try {
				
				subject.getSession();
				subject.login(token);				
				User user = userRepository.findByUsername(username);
				if (user.isActive()) {
					//List<String> roles = new ArrayList<>();
					String role = "";
					Set<String> abilitiesSet = new LinkedHashSet<>();
					role = user.getRole().getId();
					for (Ability ability : user.getRole().getRoleAbility()) {
						abilitiesSet.add(ability.getId());
					}
					
					/*for (Role role : user.getRoles()) {
						roles.add(role.getId());						
						for (Ability ability : role.getRoleAbility()) {
							abilitiesSet.add(ability.getId());							
						}
					}*/
					List<String> abilities = new ArrayList<>(abilitiesSet);
					UserInfo userInfo = new UserInfo(user.getId(), user.getUsername(), user.getFullName(), role, ip, abilities);
					userSession.setUserInfo(userInfo);					
					if (returnUrl == null || returnUrl.isEmpty()) {						
						returnUrl = "/index.xhtml";						
					}
					
					try {					
						logger.info("Redirecting to: '" + returnUrl + "'");
						facesContext.getExternalContext().redirect(
								facesContext.getExternalContext().getRequestContextPath() + returnUrl);						
					} catch (IOException e) {
						logger.error("Cannot redirect: " + e.getMessage(), e);
					}
				}
				else {
					subject.logout();
					facesContext.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_WARN, "Cannot Login", "User is inactive or doesn't have permission to login"));
					userSession.invalidateSession();
				}
			}
			catch (AuthenticationException e) {
				logger.debug(e.getMessage(), e);
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN, "Cannot Login", "Login ID or password is not match."));
				userSession.invalidateSession();
			}
		}
	}
	
	public void logout() {		
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		
		userSession.invalidateSession();
		try {
			String contextPath = facesContext.getExternalContext().getRequestContextPath();
			if (contextPath == null || contextPath.isEmpty()) {
				contextPath = "/";
			}
			facesContext.getExternalContext().redirect(contextPath);
		}
		catch (IOException e) {
			logger.error("Cannot redirect: " + e.getMessage(), e);
		}
	}

	public boolean isShowCaptcha() {
		return showCaptcha;
	}

	public void setShowCaptcha(boolean showCaptcha) {
		this.showCaptcha = showCaptcha;
	}
}
