/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.user;

import com.daksa.karyawan.entity.Ability;
import com.daksa.karyawan.entity.Role;
import com.daksa.karyawan.repository.AbilityRepository;
import com.daksa.karyawan.repository.RoleRepository;
import com.daksa.karyawan.service.RoleService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class ShowRoleBacking implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ShowRoleBacking.class);
	
	@Inject
	private RoleRepository roleRepository;
	@Inject
	private AbilityRepository abilityRepository;
	@Inject
	private RoleService roleService;
	@Inject
	private FacesContext facesContext;
	
	private List<Role> listRoles;
	private Role selectedRole;
	private List<Ability> listAbility;
	private List<Ability> selectedAbility;
	
	public void init() {
		listRoles = roleRepository.findAll();
		listAbility = abilityRepository.findAll();
	}
	
	public void selectThisRole(Role role) {
		selectedRole = role;
		selectedAbility = new ArrayList<>();
		for (Ability ability : selectedRole.getRoleAbility()) {
			selectedAbility.add(ability);
		}
	}
	
	public void editRole() {
		try {			
			selectedRole.setRoleAbility(selectedAbility);
			roleService.edit(selectedRole);		
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "User already udpated"));
			logger.info("Edit Role With Name : " + selectedRole.getName());			
		} 
		catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Failed to edit user"));			
			logger.error(e.getMessage(), e);
		}
	}

	public List<Role> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<Role> listRoles) {
		this.listRoles = listRoles;
	}

	public Role getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(Role selectedRole) {
		this.selectedRole = selectedRole;
	}

	public List<Ability> getListAbility() {
		return listAbility;
	}

	public void setListAbility(List<Ability> listAbility) {
		this.listAbility = listAbility;
	}

	public List<Ability> getSelectedAbility() {
		return selectedAbility;
	}

	public void setSelectedAbility(List<Ability> selectedAbility) {
		this.selectedAbility = selectedAbility;
	}
}
