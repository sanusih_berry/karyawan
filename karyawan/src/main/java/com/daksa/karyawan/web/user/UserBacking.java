/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.user;

import com.daksa.karyawan.entity.Role;
import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.repository.RoleRepository;
import com.daksa.karyawan.service.UserService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class UserBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(UserBacking.class);
	
	private String fullName;
	private String email;
	private List<Role> roles;
	private List<SelectItem> displayRoles;
	private String displayRole;
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private UserService userService;	
	@Inject
	private RoleRepository roleRepository;
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	private String username;

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public List<SelectItem> getDisplayRoles() {
		return displayRoles;
	}

	public void setDisplayRoles(List<SelectItem> displayRoles) {
		this.displayRoles = displayRoles;
	}

	public String getDisplayRole() {
		return displayRole;
	}

	public void setDisplayRole(String displayRole) {
		this.displayRole = displayRole;
	}

	public void init() {
		roles = roleRepository.findAll();
		displayRoles = new ArrayList<SelectItem>();
		for (Role role : roles) {
			displayRoles.add(new SelectItem(role.getId(), role.getName()));			
		}
	}	

	public void registerUser() {
		try {
			User user = new User();
			user.setEmail(email);
			user.setFullName(fullName);
			user.setPassword(username);
			user.setUsername(username);			
			Role role = new Role();
			role.setId(displayRole);			
			user.setRole(role);						
			userService.create(user);						
			cleanForm();
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "Success",
					"User already created"));			
		} 
		catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error",
					"Failed to create user"));			
		}
	}
	
	public void cleanForm() {
		email = "";
		fullName = "";
		username = "";		
	}
}
