package com.daksa.karyawan.web.table;


import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.web.DataTable;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.primefaces.model.SortOrder;



@Dependent
public class UserTable extends DataTable<User> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager entityManager;

	public UserTable() {
		super(User.class);
	}
	
	@Override
	public List<User> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		if (sortField == null) {
			sortField = "username";
			sortOrder = SortOrder.ASCENDING;
		}
		return super.load(first, pageSize, sortField, sortOrder, filters);
	}
	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
}
