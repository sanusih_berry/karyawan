/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.user;


import com.daksa.karyawan.entity.Role;
import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.repository.RoleRepository;
import com.daksa.karyawan.service.UserService;
import com.daksa.karyawan.web.table.UserTable;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class ShowUserBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ShowUserBacking.class);
	
	@Inject
	private FacesContext facesContext;
	@Inject
	private UserTable userTable;
	@Inject
	private UserService userService;
	@Inject
	private RoleRepository roleRepository;
	
	private User selectedUser;	
	private List<Role> listRoles;
	private String displayRole;
	
	
	public void init() {
		listRoles = roleRepository.findAll();
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public List<Role> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<Role> listRoles) {
		this.listRoles = listRoles;
	}

	public UserTable getUserTable() {
		return userTable;
	}

	public void setUserTable(UserTable userTable) {
		this.userTable = userTable;
	}

	public String getDisplayRole() {
		return displayRole;
	}

	public void setDisplayRole(String displayRole) {
		this.displayRole = displayRole;
	}
	
	public void selectThisUser(User user) {
		selectedUser = user;	
		displayRole = selectedUser.getRole().getId();
	}
	
	public void editUser() {
		try {			
			Role role = new Role();
			role.setId(displayRole);
			selectedUser.setRole(role);						
			userService.edit(selectedUser);
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "Success",
					"User already udpated"));
		} catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error",
					"Failed to edit user"));			
		}
	}
	
}
