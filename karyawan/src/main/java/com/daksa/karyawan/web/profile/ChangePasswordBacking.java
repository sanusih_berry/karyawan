/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.web.profile;

import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.security.UserInfo;
import com.daksa.karyawan.infrastructure.security.UserSession;
import com.daksa.karyawan.repository.UserRepository;
import com.daksa.karyawan.service.UserService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class ChangePasswordBacking implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ChangePasswordBacking.class);
	
	@Inject
	private FacesContext facesContext;

	@Inject
	private UserRepository userRepository;

	@Inject
	private UserService userService;

	@Inject
	private UserSession userSession;
		

	private String username;
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;
	private User currentUser;

	public void init() {
		UserInfo userInfo = userSession.getUserInfo();
		currentUser = userRepository.findByUsername(userInfo.getUsername());

		username = currentUser.getUsername();
	}

	public void changePassword() {
		if (oldPassword == null || oldPassword.isEmpty() || newPassword == null
				|| newPassword.isEmpty() || confirmPassword == null
				|| confirmPassword.isEmpty()) {
			facesContext
			.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"Error",
							"Please fill all input text"));
		} else {
			UsernamePasswordToken token = new UsernamePasswordToken(username,
					oldPassword);
			Subject subjectUser = SecurityUtils.getSubject();
			try {
				subjectUser.login(token);
				if (newPassword.equals(confirmPassword)) {
					currentUser.setPassword(newPassword);
					userService.edit(currentUser);
					
					facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Password already updated"));
					logger.info("Change Password");					
				} else {
					facesContext
							.addMessage(
									null,
									new FacesMessage(
											FacesMessage.SEVERITY_ERROR,
											"Error",
											"New password and confirm new password didn't match"));
				}
			} catch (AuthenticationException e) {
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Error",
						"Incorrect Password"));
				logger.debug("Failed to change password: " + e.getMessage());
				logger.error(e.getMessage(), e);
				
			} catch (Exception e) {
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Error",
						"Failed to change password"));
				logger.debug("Failed to change password: " + e.getMessage());
				logger.error(e.getMessage(), e);
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
}
