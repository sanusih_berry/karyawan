/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Berry
 */
@Entity
@Table(name = "karyawan_employee", schema = "public")
@XmlRootElement
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", length = 64)
	private String id;
	
	@Size(max = 500)
	@Column(name = "firstName", length = 500)
	private String firstName;
	
	@Size(max = 500)
	@Column(name = "lastName", length = 500)
	private String lastName;
	
	@Size(max = 500)
	@Column(name = "address", length = 500)
	private String address;
	
	@Size(max = 500)
	@Column(name = "email", length = 500)
	private String email;
	
	@Size(max = 500)
	@Column(name = "homePhone", length = 500)
	private String homePhone;
	
	@Size(max = 500)
	@Column(name = "cellPhone", length = 500)
	private String cellPhone;
	
	@Size(max = 500)
	@Column(name = "city", length = 500)
	private String city;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "karyawan_employee_position", schema = "public",
			joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "position_id", referencedColumnName = "id"))		
	private Position position;		
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
}
