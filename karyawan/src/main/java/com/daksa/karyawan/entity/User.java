package com.daksa.karyawan.entity;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.shiro.crypto.hash.Sha256Hash;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "karyawan_user", schema = "public", indexes = {
	@Index(columnList = "username", unique = true),})
@NamedQueries({
	@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u ORDER BY u.username ASC"),
	@NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
	@NamedQuery(name = "User.findByUsernameForAutoComplete", query = "SELECT u FROM User u WHERE UPPER(u.username) like :username")
})
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", nullable = false, length = 32)
	private String id;

	@Column(name = "username", length = 255, nullable = false, unique = true)
	private String username;

	@Column(name = "full_name", length = 255)
	private String fullName;

	@Column(name = "email", length = 255)
	private String email;

	@Column(name = "password", length = 64, nullable = false)
	@XmlTransient
	private String password;

	@Column(name = "password_salt", length = 64, nullable = false)
	@XmlTransient
	private String passwordSalt;

	@Column(name = "active", nullable = false)
	private boolean active;	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "karyawan_user_role", schema = "public",
			joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))		
	private Role role;

	public User() {
		this.id = UUID.randomUUID().toString().replace("-", "");
		this.active = true;
	}

	public String getUsername() {
		return username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		DefaultHashService hashService = new DefaultHashService();
		hashService.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
		hashService.setGeneratePublicSalt(true);

		DefaultPasswordService passwordService = new DefaultPasswordService();
		passwordService.setHashService(hashService);

		Hash hash = passwordService.hashPassword(password);
		this.passwordSalt = hash.getSalt().toHex();
		this.password = Hex.encodeToString(hash.getBytes());
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof User)) {
			return false;
		}
		User other = (User) object;
		return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", username=" + username + '}';
	}

	

}
