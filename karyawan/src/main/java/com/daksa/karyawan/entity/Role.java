package com.daksa.karyawan.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author @Bastiyan
 */
@Entity
@Table(name = "karyawan_role", schema = "public")
@XmlRootElement
public class Role implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", length = 32)
	private String id;

	@Column(name = "name", length = 255, nullable = false)
	private String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "karyawan_role_abilty", schema = "public",
		joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id", unique = false),
		inverseJoinColumns = @JoinColumn(name = "abilty_id", referencedColumnName = "id" , unique = false))
	private List<Ability> roleAbility;

	public Role() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Ability> getRoleAbility() {
		return roleAbility;
	}

	public void setRoleAbility(List<Ability> roleAbility) {
		this.roleAbility = roleAbility;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Role)) {
			return false;
		}
		Role other = (Role) object;
		return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
	}

	@Override
	public String toString() {
		return "Role{" + "id=" + id + ", name=" + name + '}';
	}
}
