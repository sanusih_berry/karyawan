/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ferdinand @ Daksa
 */
@Entity
@Table(name = "karyawan_ability", schema = "public")
@XmlRootElement
public class Ability implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", length = 64)
	private String id;
	@Size(max = 500)
	@Column(name = "name", length = 500)
	private String name;
	@Size(max = 500)
	@Column(name = "menu", length = 500)
	private String menu;
	@Size(max = 500)
	@Column(name = "sub_menu", length = 500)
	private String subMenu;

	public Ability() {
	}

	public Ability(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(String subMenu) {
		this.subMenu = subMenu;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Ability)) {
			return false;
		}
		Ability other = (Ability) object;
		return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
	}

	@Override
	public String toString() {
		return "Ability{" + "id=" + id + ", name=" + name + '}';
	}
}
