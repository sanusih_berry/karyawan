/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.repository;


import com.daksa.karyawan.entity.Ability;
import com.daksa.karyawan.infrastructure.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Dependent
public class AbilityRepository extends JpaRepository<Ability, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(AbilityRepository.class);
	
	@Inject
	private EntityManager entityManager;
	
	public AbilityRepository() {
		super(Ability.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
}
