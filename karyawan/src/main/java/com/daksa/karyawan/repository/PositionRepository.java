/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.repository;

import com.daksa.karyawan.entity.Position;
import com.daksa.karyawan.infrastructure.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Dependent
public class PositionRepository extends JpaRepository<Position, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(PositionRepository.class);
	
	@Inject
	private EntityManager entityManager;
	
	public PositionRepository() {
		super(Position.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public Position findByName(String name){		
		try {
			TypedQuery<Position> query = entityManager.createNamedQuery("Position.findByName", Position.class);
			query.setParameter("name", name);            
            return query.getSingleResult();
		}            
		catch (NoResultException e) {
			return null;
		}
	}
}
