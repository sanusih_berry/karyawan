/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.repository;


import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
@Dependent
public class UserRepository extends JpaRepository<User, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(UserRepository.class);
	
	@Inject
	private EntityManager entityManager;
	
	public UserRepository() {
		super(User.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public User findByUsername(String username){		
		Query query = getEntityManager().createNamedQuery("User.findByUsername");
		query.setParameter("username", username);
		try {
			return (User) query.getSingleResult();
		} 
		catch (NoResultException e) {
			return null;
		}
	}
}
