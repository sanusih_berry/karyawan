/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.service;


import com.daksa.karyawan.entity.Role;
import com.daksa.karyawan.infrastructure.util.Logging;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Berry
 */
@Stateless
public class RoleService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	@Logging
	public void create(Role r) {
		entityManager.persist(r);
	}

	@Logging
	public void edit(Role r) {
		entityManager.merge(r);
	}
}
