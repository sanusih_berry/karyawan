/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.service;

import com.daksa.karyawan.entity.Position;
import com.daksa.karyawan.exception.CreatePositionException;
import com.daksa.karyawan.infrastructure.util.Logging;
import com.daksa.karyawan.repository.PositionRepository;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Berry
 */
@Stateless
public class CreatePositionService implements Serializable {
	private static final long serialVersionUID = 1L;
		
	@Inject
	private EntityManager entityManager;
	@Inject
	private PositionRepository positionRepository;

	@Logging
	public void create(Position p) {
		entityManager.persist(p);
	}

	@Logging
	public void edit(Position p) {
		entityManager.merge(p);
	}
	
	public void createPosition(String positionName, BigDecimal basicSalary) throws CreatePositionException {
		Position existPosition = positionRepository.findByName(positionName);		
		if (existPosition == null) {
			Position position = new Position();
			position.setName(positionName);
			position.setBasicSalary(basicSalary);
			create(position);
		}
		else {
			throw new CreatePositionException("Position : " + positionName + " already exist");
		}
	}
}
