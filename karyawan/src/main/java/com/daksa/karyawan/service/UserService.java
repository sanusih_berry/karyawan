/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.service;

import com.daksa.karyawan.entity.User;
import com.daksa.karyawan.infrastructure.util.Logging;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Berry
 */
@Stateless
public class UserService implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager entityManager;
	
	@Logging
	public void create(User u){
		entityManager.persist(u);
	}
	
	@Logging
	public void edit(User u){
		entityManager.merge(u);
	}
}
