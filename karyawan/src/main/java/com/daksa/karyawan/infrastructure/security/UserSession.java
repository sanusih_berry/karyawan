package com.daksa.karyawan.infrastructure.security;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ginan
 */
@Named
@SessionScoped
public class UserSession implements Serializable {

	private static final long serialVersionUID = 1L;
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	public void invalidateSession() {
		if (FacesContext.getCurrentInstance() != null) {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
					.getExternalContext().getRequest();
			request.getSession().invalidate();
		}
		userInfo = null;
	}

	public boolean isUserHasRole(String roleId) {		
		if (userInfo != null) {						
			return userInfo.isUserHasRole(roleId);
		} else {
			return false;
		}
	}

	/*public boolean isUserHasRoles(String roleIdsInline) {
		List<String> roleIdList = Arrays.asList(roleIdsInline.trim().split("\\s*,\\s*"));
		if (userInfo != null) {
			return userInfo.isUserHasRoles(roleIdList);
		} else {
			return false;
		}
	}*/
	
	public boolean isUserHasAbility(String abilityId) {
		if (userInfo != null) {
			return userInfo.isUserHasAbility(abilityId);
		} else {
			return false;
		}
	}

	public boolean isUserHasAbilities(String abilityIdsInline) {
		List<String> roleIdList = Arrays.asList(abilityIdsInline.trim().split("\\s*,\\s*"));
		if (userInfo != null) {
			return userInfo.isUserHasAbilities(roleIdList);
		} else {
			return false;
		}
	}
}
