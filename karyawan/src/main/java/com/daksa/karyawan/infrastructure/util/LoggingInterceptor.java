package com.daksa.karyawan.infrastructure.util;

import com.daksa.karyawan.infrastructure.security.UserInfo;
import com.daksa.karyawan.infrastructure.security.UserSession;
import java.io.Serializable;

import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 *
 * @author Ginan
 */
@Interceptor
@Logging
@Dependent
public class LoggingInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private Instance<UserSession> userSessionInstance;

	@AroundInvoke
	public Object log(InvocationContext context) throws Exception {
		String userId = null;
		try {				
			UserSession userSession = userSessionInstance.get();
			if (userSession != null) {
				UserInfo user = userSession.getUserInfo();
				userId = user != null ? user.getUserId() : "";
			}
		} catch (ContextNotActiveException e) {
			// no session;
		}
		
		Class<?> methodClass = context.getMethod().getDeclaringClass();
		String className = methodClass.getName();
		String method = context.getMethod().getName();

		StringBuilder sb = new StringBuilder();
		if (userId != null) {
			sb.append("[user: ").append(userId).append("]");
		}
		sb.append(className).append(".").append(method);
		sb.append("(");
		Object[] params = context.getParameters();
		if (params != null && params.length > 0) {
			sb.append("\n\t").append(params[0]);
			for (int i = 1; i < params.length; i++) {
				sb.append(",\n\t").append(params[i]);
			}
		}
		sb.append(")");
		Logger logger = LoggerFactory.getLogger(methodClass);
		logger.debug(sb.toString());
		
		Object result = context.proceed();
		return result;
	}
}
