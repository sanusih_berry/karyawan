/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.infrastructure.persistence;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Berry
 */
@ApplicationScoped
public class EntityManagerProducer {
	@Produces
	@PersistenceContext(unitName = "karyawanPU")
	private EntityManager em;
	
	@Produces
	public Cache getCache() {
		return em.getEntityManagerFactory().getCache();
	}
}
