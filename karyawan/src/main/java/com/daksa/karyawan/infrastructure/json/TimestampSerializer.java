package com.daksa.karyawan.infrastructure.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/** * 
 * @author Ginan
 */
public class TimestampSerializer extends JsonSerializer<Date> {
	private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	
	@Override
	public void serialize(Date date, JsonGenerator jacksonGenerator, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		String serializedDate = dateFormat.format(date);
		
		jacksonGenerator.writeRawValue("\"" + serializedDate + "\"");
	}
}
