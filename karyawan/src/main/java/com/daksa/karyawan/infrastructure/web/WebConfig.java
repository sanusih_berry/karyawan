package com.daksa.karyawan.infrastructure.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan
 */
@ApplicationScoped
@Named
public class WebConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(WebConfig.class);
	
	private TimeZone timeZone;
	private Locale locale;
	private String decimalFormat;
	private String decimalFormatInTable;
	private String integerFormat;
	private String percentFormat;
	private String dateOutputFormat;
	private String dateInputFormat;
	private String timestampOutputFormat;
	private String timestampInputFormat;
	private String defaultEditorControls;
	private String uploadDirectory;
	private String productVersion;
	private String buildVersion;
	private String buildTimestamp;	
	
	public WebConfig() {
		this.timeZone = TimeZone.getDefault();
		this.locale = new Locale("en", "US");
		this.decimalFormat = "#,###.###";
		this.decimalFormatInTable = "#,##0.00";
		this.integerFormat = "#,###";
		this.percentFormat = "#,###.##";
		this.dateInputFormat = "dd-MMM-yyyy";
		this.dateOutputFormat = "dd-MMM-yyyy";
		this.timestampInputFormat = "dd-MMM-yyyy HH:mm";
		this.timestampOutputFormat = "dd-MMM-yyyy HH:mm";
		this.defaultEditorControls = "bold"
				+ " italic"
				+ " underline"
				+ " strikethrough"
				+ " font"
				+ " size"
				+ " color"
				+ " highlight"
				+ " bullets"
				+ " numbering"
				+ " alignleft"
				+ " center"
				+ " alignright"
				+ " justify"
				+ " image"
				+ " link"
				+ " unlink"
				+ " outdent"
				+ " indent";
		this.uploadDirectory = System.getProperty("user.home") 
				+ FileSystems.getDefault().getSeparator() + "qpm" 
				+ FileSystems.getDefault().getSeparator() + "upload" 
				+ FileSystems.getDefault().getSeparator(); 
				
		try (InputStream in = WebConfig.class.getClassLoader().getResourceAsStream("buildInfo.properties")) {
			Properties properties = new Properties();
			properties.load(in);
			this.productVersion = properties.getProperty("product.version");
			this.buildVersion = properties.getProperty("build.version");
			this.buildTimestamp = properties.getProperty("build.timestamp");
		} catch (IOException e) {
			logger.error("Error loading buildInfo.properties", e);
			throw new RuntimeException(e);
		}
		
		//generateHeaderForPerformanceArea();
	}
	
	/*private void generateHeaderForPerformanceArea(){
		this.listHeadPerformanceArea = new ArrayList<>();
		HeadPerformanceAreaModel headCommercial = new HeadPerformanceAreaModel();
		headCommercial.setSegment("Commercial Banking");
		headCommercial.setShowNiiLending(true);
		headCommercial.setTotalSpan(3);
		headCommercial.setIndex(0);
		List<FeeHeadPerformanceAreaModel> listFeeCommercial = new ArrayList<>();
		listFeeCommercial.add(new FeeHeadPerformanceAreaModel("FX", true, 0));
		listFeeCommercial.add(new FeeHeadPerformanceAreaModel("Advisory", true, 1));
		headCommercial.setListFee(listFeeCommercial);
		
		HeadPerformanceAreaModel headQnbFirst = new HeadPerformanceAreaModel();
		headQnbFirst.setSegment("QNB First");
		headQnbFirst.setTotalSpan(3);
		headQnbFirst.setIndex(1);
		List<FeeHeadPerformanceAreaModel> listFeeQnbFirst = new ArrayList<>();
		listFeeQnbFirst.add(new FeeHeadPerformanceAreaModel("FX", true, 0));
		listFeeQnbFirst.add(new FeeHeadPerformanceAreaModel("Mutual Fund", true, 2));
		listFeeQnbFirst.add(new FeeHeadPerformanceAreaModel("Banca", true, 1));
		headQnbFirst.setListFee(listFeeQnbFirst);
		
		HeadPerformanceAreaModel headRetailBanking = new HeadPerformanceAreaModel();
		headRetailBanking.setSegment("Retail Banking");
		headRetailBanking.setShowNiiLending(true);
		headRetailBanking.setShowNiiFunding(true);
		headRetailBanking.setTotalSpan(4);
		headRetailBanking.setIndex(2);
		List<FeeHeadPerformanceAreaModel> listFeeRetail = new ArrayList<>();
		listFeeRetail.add(new FeeHeadPerformanceAreaModel("FX", true, 0));
		listFeeRetail.add(new FeeHeadPerformanceAreaModel("Banca", true, 1));
		headRetailBanking.setListFee(listFeeRetail);
		
		HeadPerformanceAreaModel headWholesaleFunding = new HeadPerformanceAreaModel();
		headWholesaleFunding.setSegment("Wholesale Funding");
		headWholesaleFunding.setShowNiiFunding(true);
		headWholesaleFunding.setTotalSpan(1);
		headWholesaleFunding.setIndex(3);
		
		this.listHeadPerformanceArea.add(headCommercial);
		this.listHeadPerformanceArea.add(headQnbFirst);
		this.listHeadPerformanceArea.add(headRetailBanking);
		this.listHeadPerformanceArea.add(headWholesaleFunding);
		
		mapHeadPerformanceArea = new HashMap<>();
		for(HeadPerformanceAreaModel head : listHeadPerformanceArea){
			mapHeadPerformanceArea.put(head.getSegment(), head);
		}
	}*/

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public Locale getLocale() {
		return locale;
	}

	public String getDecimalFormat() {
		return decimalFormat;
	}
	
	public String getDecimalFormatInTable() {
		return decimalFormatInTable;
	}
	
	public String getIntegerFormat() {
		return integerFormat;
	}
	
	public String getPercentFormat() {
		return percentFormat;
	}

	public String getDateOutputFormat() {
		return dateOutputFormat;
	}

	public String getDateInputFormat() {
		return dateInputFormat;
	}

	public String getTimestampOutputFormat() {
		return timestampOutputFormat;
	}

	public String getTimestampInputFormat() {
		return timestampInputFormat;
	}

	public String getDefaultEditorControls() {
		return defaultEditorControls;
	}
	
	public String getUploadDirectory() {
		return uploadDirectory;
	}
	
	public String getProductVersion() {
		return productVersion;
	}
	
	public String getBuildTimestamp() {
		return buildTimestamp;
	}
	
	public String getBuildVersion() {
		return buildVersion;
	}

	/*public List<HeadPerformanceAreaModel> getListHeadPerformanceArea() {
		return listHeadPerformanceArea;
	}

	public Map<String, HeadPerformanceAreaModel> getMapHeadPerformanceArea() {
		return mapHeadPerformanceArea;
	}*/	
}
