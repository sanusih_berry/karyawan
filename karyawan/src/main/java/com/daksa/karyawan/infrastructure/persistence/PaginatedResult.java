package com.daksa.karyawan.infrastructure.persistence;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PaginatedResult<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private List<T> items;
    private int pageSize;
    private int pageNumber;
    private int pagesCount;
    private long totalItemsCount;

	public PaginatedResult() {
		this.items = null;
		this.pageSize = 0;
		this.pageNumber = 0;
		this.pagesCount = 0;
		this.totalItemsCount = 0;
	}

    public PaginatedResult(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        items = Collections.emptyList();
        pagesCount = 0;
        totalItemsCount = 0;
    }

    public PaginatedResult(List<T> items, int pageNumber, int pageSize, long totalItemsCount) {
        this.items = items;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.pagesCount = countPages(pageSize, totalItemsCount);
        this.totalItemsCount = totalItemsCount;
    }

    private int countPages(int size, long itemsCount) {
        return (int) Math.ceil((double) itemsCount / size);
    }

    public List<T> getItems() {
        return items;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public long getTotalItemsCount() {
        return totalItemsCount;
    }
}