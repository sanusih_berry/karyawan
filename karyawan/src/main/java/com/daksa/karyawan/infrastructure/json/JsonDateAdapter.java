package com.daksa.karyawan.infrastructure.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * XmlAdapter untuk tipe {@code Date} (hanya sampai {@link Calendar#DATE} 
 * sementara {@link Calendar#HOUR_OF_DAY} kebawah diabaikan) 
 * yang digunakan oleh lib ini.
 * 
 * @author Ginan
 * @see JsonTimestampAdapter
 */
public class JsonDateAdapter extends XmlAdapter<String, Date> {
	
	/**
	 * Format tanggal yang dipakai.
	 */
	private final static String DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * Dari representasi tekstual ke objek.
	 * 
	 * @param v Representasi tekstualnya
	 * @return Objeknya
	 * @throws Exception Bilamana terjadi kegagalan proses konversi.
	 */
	@Override
	public Date unmarshal(String v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.parse(v);
	}

	/**
	 * Dari objek ke representasi tekstualnya.
	 * 
	 * @param v Objeknya
	 * @return Representasi tekstualnya
	 * @throws Exception Bilamana terjadi kegagalan proses konversi.
	 */
	@Override
	public String marshal(Date v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.format(v);
	}
	
}
