package com.daksa.karyawan.infrastructure.persistence;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Ginan
 * @param <E> Entity class
 * @param <K> Entity primary key class
 */
public abstract class JpaRepository<E, K> {

	private final Class<E> entityClass;

	public JpaRepository(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	public E find(K id) {
		if (id != null) {
			return getEntityManager().find(entityClass, id);
		} else {
			return null;
		}
	}

	public List<E> findAll() {
		return findAll(null);
	}
	
	public List<E> findAll(PaginatedParam paginatedParam) {
		TypedQuery<E> queryFind = getEntityManager().createQuery(
				"SELECT e FROM " + getEntityName() + " e", entityClass);
		if (paginatedParam != null) {
			queryFind.setFirstResult(paginatedParam.getOffset());
			queryFind.setMaxResults(paginatedParam.getPageSize());
		}
		List<E> result = queryFind.getResultList();
		return result;
	}

	public abstract EntityManager getEntityManager();
	
	protected String getEntityName() {
		return entityClass.getSimpleName();
	}
}
