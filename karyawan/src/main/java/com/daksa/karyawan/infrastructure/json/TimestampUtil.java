package com.daksa.karyawan.infrastructure.json;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Utility class untuk konversi dari beberapa format {@link Date} string ke
 * {@code Date} object, serta konversi dari dan ke format datetime string XML
 * (XSD) timestamp. Daftar di bawah ini menjabarkan format date/time string yang
 * diterima:
 * <ul>
 * <li>{@code yyyy-MM-dd}</li>
 * <li>{@code yyyy-MM-dd HH:mm:ss}</li>
 * <li>{@code yyyy-MM-dd HH:mm:ss.SSS}</li>
 * <li>{@code HH:mm:ss}</li>
 * <li>{@code HH:mm:ss.SSS}</li>
 * </ul>
 * serta dua format spesifik XML/XSD timestamp:
 * <ul>
 * <li>{@code yyyy-MM-dd'T'HH:mm:ss.SSS}</li>
 * <li>{@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}</li>
 * </ul>
 *
 * Perhatikan bahwa konversi dari string ke {@code Date} membolehkan adanya
 * trailing whitespaces (mengikuti perilaku {@link
 * DateFormat#parse(java.lang.String)}), serta bersifat {@link
 * DateFormat#isLenient} = {@code false} (sebab dikenai proses regex
 * pre-matching).
 *
 * @see SimpleDateFormat
 * @see SimpleDateFormat#parse(java.lang.String, java.text.ParsePosition)
 * @deprecated 
 * @author Ginan
 */
public class TimestampUtil {

	/**
	 * {@code yyyy-MM-dd'T'HH:mm:ss.SSS}<br />
	 * ex. 2012-02-03T04:05:06.432
	 */
	public static final String XML_TIMESTAMP_WITHOUT_TIMEZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

	/**
	 * {@code yyyy-MM-dd'T'HH:mm:ss.SSSZ}<br />
	 * ex. 2012-02-03T04:05:06.432+07:00
	 */
	public static final String XML_TIMESTAMP_WITH_TIMEZONE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	/**
	 * Pemetaan dari {@link Pattern} yang bersesuaian ke masing-masing format
	 * datetime seperti yang dijabarkan di {@link TimestampUtil}
	 */
	private static final Map<Pattern, String> DATE_PATTERN_MAP = new HashMap<Pattern, String>();

	static {
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\s*$"
				), "yyyy-MM-dd HH:mm:ss"
		);
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\.\\d{3}\\s*$"
				), "yyyy-MM-dd HH:mm:ss.SSS"
		);
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{4}-\\d{2}-\\d{2}\\s*$"
				), "yyyy-MM-dd"
		);
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{2}:\\d{2}:\\d{2}\\s*$"
				), "HH:mm:ss"
		);
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{2}:\\d{2}:\\d{2}\\.\\d{3}\\s*$"
				), "HH:mm:ss.SSS"
		);
		DATE_PATTERN_MAP.put(
				Pattern.compile(
						"^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}"
						+ "([+\\-]\\d{2}(:?)\\d{2})?\\s*$"
				), null
		);
	}

	/**
	 * Posisi penyisipan atau pengecekan karakter ':' pada format timestamp
	 * dengan timezone.
	 */
	private static final int TIMEZONE_COLON_INDEX = 26;

	/**
	 * Melakukan konversi dari bentuk timestamp XML menjadi {@code Date}. Contoh
	 * format timestamp XML adalah 2011-08-05T23:15:00.123+07:00
	 *
	 * @param timestampString String timestamp dengan format seperti yang
	 * tertulis di {@link TimestampUtil} (bagian XML/XSD format).
	 * @return date {@code Date} object
	 * @throws ParseException Bila gagal parsing string masukan.
	 */
	public static Date parseXmlTimestamp(String timestampString)
			throws ParseException {
		String timestamp = timestampString.trim();
		int indexDotMiliseconds = timestamp.indexOf(".");
		if (indexDotMiliseconds == -1 || indexDotMiliseconds > 19) {
			StringBuilder sb = new StringBuilder(timestamp);
			if (timestamp.length() > 19) {
				sb.insert(19, ".000");
			} else {
				sb.append(".000");
			}
			timestamp = sb.toString();
		}
		int length = 23; // length tanpa timezone
		int n = timestamp.length();
		StringBuilder sb = new StringBuilder(timestamp);
		if (n > TIMEZONE_COLON_INDEX) {
			if (sb.charAt(TIMEZONE_COLON_INDEX) == ':') {
				sb.replace(TIMEZONE_COLON_INDEX, TIMEZONE_COLON_INDEX + 1, "");
			}
		}

		String format;
		if (n > length) {
			format = XML_TIMESTAMP_WITH_TIMEZONE_FORMAT;
		} else {
			format = XML_TIMESTAMP_WITHOUT_TIMEZONE_FORMAT;
		}

		return new SimpleDateFormat(format).parse(sb.toString());
	}

	/**
	 * Melakukan konversi dari objek {@link Date} menjadi XML timestamp.
	 *
	 * @param date {@code Date} yang akan dikonversi.
	 * @param withTimezone {@code true} bila informasi timezone di dalam
	 * {@code Date} masukan juga diperhitungkan.
	 * @return XML timestamp, dengan atau tanpa timezone sesuai nilai {@code
	 * withTimezone}.
	 */
	public static String formatXmlTimestamp(Date date, boolean withTimezone) {
		StringBuilder sb = new StringBuilder();
		if (withTimezone) {
			DateFormat dateFormat = new SimpleDateFormat(XML_TIMESTAMP_WITH_TIMEZONE_FORMAT);
			sb.append(dateFormat.format(date));
			sb.insert(TIMEZONE_COLON_INDEX, ":");
		} else {
			DateFormat dateFormat = new SimpleDateFormat(XML_TIMESTAMP_WITHOUT_TIMEZONE_FORMAT);
			sb.append(dateFormat.format(date));
		}
		return sb.toString();
	}

	/**
	 * Melakukan konversi dari objek {@link Date} menjadi XML timestamp.
	 *
	 * @param date {@code Date} yang akan dikonversi.
	 * @param timezone {@link TimeZone} yang diinginkan, bila {@code null} maka
	 * keluaran tanpa timezone.
	 * @return XML timestamp, dengan atau tanpa timezone sesuai nilai {@code
	 * timezone}.
	 */
	public static String formatXmlTimestamp(Date date, TimeZone timezone) {
		if (timezone == null) {
			DateFormat dateFormat = new SimpleDateFormat(XML_TIMESTAMP_WITHOUT_TIMEZONE_FORMAT);
			return dateFormat.format(date);
		} else {
			Calendar instance = Calendar.getInstance();
			instance.setTime(date);
			instance.setTimeZone(timezone);

			DateFormat dateFormat = new SimpleDateFormat(XML_TIMESTAMP_WITH_TIMEZONE_FORMAT);

			StringBuilder sb = new StringBuilder();
			sb.append(dateFormat.format(instance.getTime()));
			sb.insert(TIMEZONE_COLON_INDEX, ":");
			return sb.toString();
		}
	}

	/**
	 * Melakukan konversi dari string timestamp menjadi {@link Date}. Format
	 * string timestamp yang valid dapat dilihat di {@link TimestampUtil}.
	 *
	 * @param value String timestamp masukan.
	 * @return Objek {@code Date}.
	 * @throws ParseException Bilamana gagal parsing string masukan menggunakan
	 * semua format yang dibolehkan.
	 */
	public static Date parseDate(String value) throws ParseException {
		Date date = null;
		String format = null;
		boolean match = false;
		for (Pattern pattern : DATE_PATTERN_MAP.keySet()) {
			if (pattern.matcher(value).matches()) {
				format = DATE_PATTERN_MAP.get(pattern);
				match = true;
				break;
			}
		}

		if (!match) {
			throw new ParseException("Invalid date format", 0);
		} else if (format == null) {
			date = parseXmlTimestamp(value);
		} else {
			DateFormat dateFormat = new SimpleDateFormat(format);
			date = dateFormat.parse(value);
		}
		return date;
	}
}
