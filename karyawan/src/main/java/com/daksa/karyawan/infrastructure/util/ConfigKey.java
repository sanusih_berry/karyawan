/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.infrastructure.util;

/**
 *
 * @author Ferdinand @ Daksa
 */
public enum ConfigKey {
	USERNAME_FTP , PASSWORD_FTP , URL_HOST_FTP , PORT_FTP,
	
	URL_HOST_PERCETAKAN , USERNAME_PERCETAKAN , PASSWORD_PERCETAKAN , PORT_PERCETAKAN,
	
	SMTP_HOST , SMTP_PORT , EMAIL_FROM
}
