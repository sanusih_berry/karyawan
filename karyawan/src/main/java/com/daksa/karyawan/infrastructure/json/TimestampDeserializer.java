package com.daksa.karyawan.infrastructure.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Ginan
 */
public class TimestampDeserializer extends JsonDeserializer<Date> {
	private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	
	@Override
	public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
			throws IOException, JsonProcessingException {
		try {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			Date date = dateFormat.parse(jsonParser.getText());
			return date;
		} catch (ParseException e) {
			throw new JsonParseException(e.getMessage(), jsonParser.getCurrentLocation());
		}
	}
}
