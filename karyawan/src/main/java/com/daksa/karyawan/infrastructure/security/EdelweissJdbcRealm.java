package com.daksa.karyawan.infrastructure.security;

import java.io.UnsupportedEncodingException;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.util.ByteSource;

public class EdelweissJdbcRealm extends JdbcRealm {
	public EdelweissJdbcRealm() {
		setSaltStyle(SaltStyle.COLUMN);
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {
		SimpleAuthenticationInfo info = (SimpleAuthenticationInfo)super.doGetAuthenticationInfo(token);
		try {
			if (info.getCredentialsSalt() != null) {
				String saltHex = new String(info.getCredentialsSalt().getBytes(), "UTF-8");
				info.setCredentialsSalt(ByteSource.Util.bytes(Hex.decode(saltHex)));
			}
			return info;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}

	}
}
