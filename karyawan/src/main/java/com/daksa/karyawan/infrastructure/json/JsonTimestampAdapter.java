package com.daksa.karyawan.infrastructure.json;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * XmlAdapter untuk tipe {@code Date} yang digunakan oleh lib ini.
 *
 * @author Ginan
 * @see TimestampUtil
 */
public class JsonTimestampAdapter extends XmlAdapter<String, Date> {
	
	/**
	 * Dari representasi tekstual ke objek.
	 * 
	 * @param v Representasi tekstualnya
	 * @return Objeknya
	 * @throws Exception Bilamana terjadi kegagalan proses konversi.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public Date unmarshal(String v) throws Exception {
		return TimestampUtil.parseXmlTimestamp(v);
	}

	/**
	 * Dari objek ke representasi tekstualnya (selalu <b>dengan</b> field 
	 * timezone nya)
	 * 
	 * @param v Objeknya
	 * @return Representasi tekstualnya
	 * @throws Exception Bilamana terjadi kegagalan proses konversi.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String marshal(Date v) throws Exception {
		return TimestampUtil.formatXmlTimestamp(v, true);
	}
	
}
