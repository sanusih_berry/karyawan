package com.daksa.karyawan.infrastructure.persistence;

/**
 *
 * @author Ginan
 */
public class PaginatedParam {

	private final int pageSize;
	private final int pageNumber;

	private PaginatedParam(int pageNumber, int pageSize) {
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getOffset() {
		return offsetFromPageNumber(pageNumber, pageSize);
	}
	
	public static int pageNumberFromOffset(int offset, int pageSize) {
		return offset / pageSize + 1;
	}
	
	public static int offsetFromPageNumber(int pageNumber, int pageSize) {
		return (pageNumber - 1) * pageSize;
	}
	
	public static PaginatedParam fromOffset(int offset, int pageSize) {
		return new PaginatedParam(
				pageNumberFromOffset(offset, pageSize),
				pageSize);
	}
	
	public static PaginatedParam fromPageNumber(int pageNumber, int pageSize) {
		return new PaginatedParam(pageNumber, pageSize);
	}
}
