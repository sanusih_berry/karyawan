package com.daksa.karyawan.infrastructure.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Ginan
 * @param <E>
 */
public abstract class DataTable<E> extends LazyDataModel<E> {
	private static final long serialVersionUID = 1L;
	
	private Class<E> entityClass;
	private String entityName;
	private String entityVar;
	private String selectQuery;
	private String countQuery;

	protected DataTable() {
		this.entityVar = "e";
	}
	
	public DataTable(Class<E> entityClass) {
		this.entityClass = entityClass;
		this.entityName = entityClass.getSimpleName();
		this.entityVar = generateEntityVar(this.entityName);

		// SELECT-FROM-WHERE
		this.selectQuery = "SELECT " + entityVar
				+ " FROM " + entityName + " " + entityVar
				+ " WHERE TRUE = TRUE";

		// SELECT-COUNT-FROM-WHERE
		this.countQuery = "SELECT COUNT(" + entityVar + ")"
				+ " FROM " + entityName + " " + entityVar
				+ " WHERE TRUE = TRUE";
	}

	private String generateEntityVar(String entityName) {
		return entityName.substring(0, 1).toLowerCase(Locale.getDefault()) + entityName.substring(1);
	}

	@Override
	public List<E> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		StringBuilder filterBuilder = new StringBuilder(64);
		List<Object> params = new ArrayList<>();
		if (filters != null && !filters.isEmpty()) {
			int i = 0;
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = "" + entry.getValue();
				
				FilterItem filterItem = createFilterItem(
						entry.getKey(),
						queryVar(entry.getKey()),
						value);

				// AND (operand operator ?i)
				filterBuilder.append(" AND (")
						.append(filterItem.getOperand())
						.append(" ").append(filterItem.getOperator())
						.append(" :").append(getEntityVar()).append(i).append(")");
				params.add(filterItem.getValue());
				i++;
			}
		}

		String sorting1 = getSortingFirst();
		String sorting2 = getSortingLast();
		
		StringBuilder sortingBuilder = new StringBuilder();
		if (sorting1 != null) {
			sortingBuilder.append(sorting1);
		}
		if (sortField != null && !sortField.isEmpty()) {
			if (sortingBuilder.length() > 0) {
				sortingBuilder.append(", ");
			}
			sortingBuilder.append(queryVar(sortField))
					.append(" ")
					.append(sortOrder == SortOrder.ASCENDING ? "ASC" : "DESC");
		}
		if (sorting2 != null) {
			if (sortingBuilder.length() > 0) {
				sortingBuilder.append(", ");
			}
			sortingBuilder.append(sorting2);
		}
		
		String sorting = "";
		if (sortingBuilder.length() > 0) {
			sorting = " ORDER BY " + sortingBuilder.toString();
		}

		String filterStr = filterBuilder.toString();
		String queryFindStr = getSelectQuery() + filterStr + (getSelectGroupBy() != null ? (" GROUP BY " + getSelectGroupBy()) : "") + sorting;
		String queryCountStr = getCountQuery() + filterStr + (getCountGroupBy()!= null ? (" GROUP BY " + getCountGroupBy()) : "");

		TypedQuery<E> queryFind = getEntityManager().createQuery(queryFindStr, entityClass);
		TypedQuery<Long> queryCount = getEntityManager().createQuery(queryCountStr, Long.class);

		queryFind.setMaxResults(pageSize);
		queryFind.setFirstResult(first);

		int i = 0;
		for (Object param : params) {
			queryFind.setParameter(getEntityVar() + i, param);
			queryCount.setParameter(getEntityVar() + i, param);
			i++;
		}

		if (getAdditionalParams() != null) {
			for (Map.Entry<String, Object> entry : getAdditionalParams().entrySet()) {
				queryFind.setParameter(entry.getKey(), entry.getValue());
				queryCount.setParameter(entry.getKey(), entry.getValue());
			}
		}

		List<E> result = queryFind.getResultList();
		Long count;
		try {
			count = queryCount.getSingleResult();
		} catch (NoResultException e) {
			count = 0L;
		}
		setRowCount(count.intValue());

		return result;
	}

	private String queryVar(String param) {
		if (param.startsWith(":")) {
			return param.substring(1);
		} else {
			return getEntityVar() + "." + param;
		}
	}
	
	protected FilterItem createFilterItem(String fieldName, String operand, String valueStr) {
		return FilterItem.stringLikeFilter(operand, valueStr);
	}

	protected String getEntityVar() {
		return entityVar;
	}

	protected String getEntityName() {
		return entityName;
	}

	protected String getSelectQuery() {
		return selectQuery;
	}

	protected String getCountQuery() {
		return countQuery;
	}

	protected Map<String, Object> getAdditionalParams() {
		return null;
	}
	
	protected String getSelectGroupBy() {
		return null;
	}
	
	protected String getCountGroupBy() {
		return null;
	}
	
	protected String getSortingFirst() {
		return null;
	}
	
	protected String getSortingLast() {
		return null;
	}

	protected abstract EntityManager getEntityManager();

	public static class FilterItem {

		private final String operator;
		private final String operand;
		private final Object value;

		public FilterItem(String operand, String operator, Object value) {
			this.operator = operator;
			this.operand = operand;
			this.value = value;
		}

		public String getOperator() {
			return operator;
		}

		public String getOperand() {
			return operand;
		}

		public Object getValue() {
			return value;
		}

		public static FilterItem booleanFilter(String operand, String valueStr) {
			return new FilterItem(operand, " = ", Boolean.valueOf(valueStr));
		}

		public static FilterItem stringLikeFilter(String operand, String valueStr) {
			return new FilterItem(
					"lower(" + operand + ")",
					"LIKE",
					"%" + valueStr.toLowerCase(Locale.getDefault()) + "%");
		}

		public static FilterItem stringMatchFilter(String operand, String valueStr) {
			return new FilterItem(operand, " = ", valueStr);
		}
	}
}
