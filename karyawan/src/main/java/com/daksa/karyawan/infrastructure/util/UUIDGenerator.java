package com.daksa.karyawan.infrastructure.util;

import java.util.UUID;

/**
 *
 * @author Ginan
 */
public class UUIDGenerator {
	/**
	 * Generate 32 characters of UUID
	 * @return UUID
	 */
	public static String generate() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
