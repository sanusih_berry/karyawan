/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.karyawan.infrastructure.util;

/**
 *
 * @author Ferdinand @ Daksa
 */
public enum EventTypeEnum {
	FTP_PERCETAKAN , EMAIL , FTP , REPORT,
	
	FETCH_NASABAH , FETCH_TABUNGAN , FETCH_DEPOSITO , FETCH_MUTATION_SAVING , FETCH_KURS,
	
	FETCH_REKSADANA, FETCH_REKSADANA_HISTORY,
	
	FETCH_AIA, FTP_AIA,
	
	FETCH_DICIONARY,
	
	CREATE_REPORT
}
