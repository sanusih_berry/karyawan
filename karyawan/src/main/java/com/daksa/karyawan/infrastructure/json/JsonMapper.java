package com.daksa.karyawan.infrastructure.json;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.TimestampDeserializer;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

/**
 *
 * @author Ginan
 */
public class JsonMapper {

	/**
	 *
	 * @return ObjectMapper
	 */
	public static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();

		AnnotationIntrospector jaxbInterospector = new JaxbAnnotationIntrospector(TypeFactory.defaultInstance());
		AnnotationIntrospector jacksonInterospector = new JacksonAnnotationIntrospector();
		mapper.setAnnotationIntrospector(AnnotationIntrospector.pair(jaxbInterospector, jacksonInterospector));

		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);

		SimpleModule modul = new SimpleModule();
		modul.addSerializer(Date.class, new TimestampSerializer());
		modul.addDeserializer(Date.class, new TimestampDeserializer());
		mapper.registerModule(modul);

		return mapper;
	}

	public static String writePrettyJson(Object object) {
		try {
			return getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
