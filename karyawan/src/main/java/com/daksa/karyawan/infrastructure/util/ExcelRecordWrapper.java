package com.daksa.karyawan.infrastructure.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelRecordWrapper {
	private static final Logger logger = LoggerFactory.getLogger(ExcelRecordWrapper.class);
	private static final char DEFAULT_DECIMAL_SEPARATOR = '.';
	private static final char DEFAULT_GROUPING_SEPARATOR = ',';
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	private List<String> fields;
	private Row row;
	private FormulaEvaluator evaluator;
	
	public ExcelRecordWrapper(Row row) {
		this.row = row;
	}

	public ExcelRecordWrapper(List<String> fields, Row row) {
		this.fields = fields;
		this.row = row;
	}
	
	public ExcelRecordWrapper(List<String> fields, Row row, FormulaEvaluator evaluator) {
		this.fields = fields;
		this.row = row;
		this.evaluator = evaluator;
	}
	
	public String getString(String field) {
		if (fields != null && !fields.isEmpty()) {
			int idx = fields.indexOf(field);
			if (idx >= 0) {
				Cell cell = row.getCell(idx);
				return getDisplayedValue(cell);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public String getString(int field) {
		Cell cell = row.getCell(field);
		return getDisplayedValue(cell);
	}
	
	public Double getDouble(String field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return convertNumber(value).doubleValue();
		} else {
			return null;
		}
	}
	
	public Double getDouble(int field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return convertNumber(value).doubleValue();
		} else {
			return null;
		}
	}
	
	public Integer getInteger(String field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return convertNumber(value).intValue();
		} else {
			return null;
		}
	}
	
	public Integer getInteger(int field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return convertNumber(value).intValue();
		} else {
			return null;
		}
	}
	
	public BigDecimal getBigDecimal(String field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return new BigDecimal(convertNumber(value).doubleValue(), MathContext.DECIMAL64);
		} else {
			return null;
		}
	}
	
	public BigDecimal getBigDecimal(int field) throws ParseException {
		String value = getString(field);
		if (value != null && !value.isEmpty()) {
			return new BigDecimal(convertNumber(value).doubleValue(), MathContext.DECIMAL64);
		} else {
			return null;
		}
	}

	public Date getDate(String field) {
		Date date = null;
		if (fields != null && !fields.isEmpty()) {
			int idx = fields.indexOf(field);
			if (idx >= 0) {
				Cell cell = row.getCell(idx);
				if (cell != null) {
					if (DateUtil.isCellDateFormatted(cell)) {
						date = cell.getDateCellValue();
					} else {
						String dateString = getDisplayedValue(cell);
						if (dateString != null && !dateString.isEmpty()) {
							try {
								DateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);
								date = dateFormat.parse(dateString);
							} catch (ParseException e) {
								logger.warn("Cannot parse date '{}' in field '{}'.", dateString, field);
							}
						}
					}
				}
			}
		}
		return date;
	}
	
	public Date getDate(int field) {
		Date date = null;
		if (fields != null && !fields.isEmpty()) {
			Cell cell = row.getCell(field);
			if (cell != null) {
				if (DateUtil.isCellDateFormatted(cell)) {
					date = cell.getDateCellValue();
				} else {
					String dateString = getDisplayedValue(cell);
					try {
						DateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);
						date = dateFormat.parse(dateString);
					} catch (ParseException e) {
						logger.warn("Cannot parse date '{}' in field '{}'.", dateString, field);
					}
				}
			}
		}
		return date;
	}
	
	private String getDisplayedValue(Cell cell) {
		String cellValue = null;
		if (cell != null) {
			int cellType = cell.getCellType();
			if (evaluator != null) {
				int evaluatedCellType = evaluator.evaluateFormulaCell(cell);
				if (evaluatedCellType != -1) {
					cellType = evaluatedCellType;
				}
			}
			switch (cellType) {
				case Cell.CELL_TYPE_STRING:
					cellValue = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (DateUtil.isCellDateFormatted(cell)) {
						cellValue = cell.getDateCellValue().toString();
					} else {
						cellValue = Double.toString(cell.getNumericCellValue());

						// Hack, convert xxx.0 menjadi xxx
						if (cellValue != null && cellValue.endsWith(".0")) {
							cellValue = cellValue.substring(0, cellValue.length() - 2);
						}
					}
					break;
				case Cell.CELL_TYPE_BLANK:
					cellValue = null;
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					cellValue = Boolean.toString(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_FORMULA:
					cellValue = cell.getCellFormula();
					break;
			}

			if (cellValue != null) {
				cellValue = cellValue.trim();
				if (cellValue.isEmpty()) {
					cellValue = null;
				}
			}
		}
		return cellValue;
	}
	
	private Number convertNumber(String data) throws ParseException{
		DecimalFormat decimalFormat = new DecimalFormat();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(DEFAULT_DECIMAL_SEPARATOR);
		symbols.setGroupingSeparator(DEFAULT_GROUPING_SEPARATOR);
		decimalFormat.setDecimalFormatSymbols(symbols);
		return decimalFormat.parse(data);
	}
}
