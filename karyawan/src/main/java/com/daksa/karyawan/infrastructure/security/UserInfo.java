package com.daksa.karyawan.infrastructure.security;

import java.util.Iterator;
import java.util.List;

public class UserInfo {
	private String userId;
	private String username;
	private String fullName;
	private String remoteAddress;
	private String role;
	private String accountOfficerId;
	private String accountOfficerPosition;
	private List<String> abilities;

	public UserInfo() {
	}

	public UserInfo(String userId, 
			String username, 
			String fullName,
			String role, 
			String remoteAddress,
			List<String> abilities) {
		this.userId = userId;
		this.username = username;
		this.fullName = fullName;
		this.remoteAddress = remoteAddress;
		this.role = role;
		this.abilities = abilities;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAccountOfficerId() {
		return accountOfficerId;
	}

	public void setAccountOfficerId(String accountOfficerId) {
		this.accountOfficerId = accountOfficerId;
	}

	public String getAccountOfficerPosition() {
		return accountOfficerPosition;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setAccountOfficerPosition(String accountOfficerPosition) {
		this.accountOfficerPosition = accountOfficerPosition;
	}

	public boolean isUserHasRole(String roleId) {
		if (role.equals(roleId)) {
			return true;
		} else {
			return false;
		}
	}
	
	/*public boolean isUserHasRoles(List<String> roleIdList) {
		boolean found = false;
		if (roles != null) {
			Iterator<String> i = roleIdList.iterator();
			while (i.hasNext() && !found) {
				String roleId = i.next();
				if (isUserHasRole(roleId)) {
					found = true;
				}
			}
		}
		return found;
	}*/
	
	public boolean isUserHasAbility(String ability) {
		if (abilities != null) {
			return abilities.contains(ability);
		} 
		else {
			return false;
		}
	}
	
	public boolean isUserHasAbilities(List<String> abilities) {
		boolean found = false;
		if (abilities != null) {
			Iterator<String> i = abilities.iterator();
			while (i.hasNext() && !found) {
				String ability = i.next();
				if (isUserHasAbility(ability)) {
					found = true;
				}
			}
		}
		return found;
	}
}
