package com.daksa.karyawan.infrastructure.security;

import java.io.IOException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;

/**
 *
 * @author Ginan
 */
public class EdelweissRolesAuthzFilter extends RolesAuthorizationFilter {

	@Override
	public boolean isAccessAllowed(ServletRequest request, ServletResponse response,
			Object mappedValue) throws IOException {
		final Subject subject = getSubject(request, response);
		final String[] roles = (String[]) mappedValue;

		boolean allowed = false;
		if (roles == null || roles.length == 0) {
			allowed = true;
		} else {
			int i = 0;
			while (!allowed && i < roles.length) {
				allowed = subject.hasRole(roles[i++]);
			}
		}
		return allowed;
	}
}
