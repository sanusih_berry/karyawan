package com.daksa.karyawan.infrastructure.security;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.PassThruAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

/**
 *
 * @author Ginan
 */
public class EdelweissPassThruAuthcFilter extends PassThruAuthenticationFilter {
	 private static final String FACES_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	            + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";
	@Override
	protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String returnUrl = httpServletRequest.getRequestURI();
		String param = httpServletRequest.getQueryString();
		
		if (httpServletRequest.getContextPath() != null
				&& !httpServletRequest.getContextPath().isEmpty()
				&& returnUrl.startsWith(httpServletRequest.getContextPath())) {
			returnUrl = returnUrl.substring(httpServletRequest.getContextPath().length()).trim();
		}
		
		if (param != null && !param.isEmpty()) {
			returnUrl += "?" + param;
		}
		
		httpServletRequest.getSession().setAttribute("returnUrl", returnUrl);
		if ("partial/ajax".equals(httpServletRequest.getHeader("Faces-Request"))) {
			response.setContentType("text/xml");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().printf(FACES_REDIRECT_XML, httpServletRequest.getContextPath() + getLoginUrl());
		} else {			
			WebUtils.issueRedirect(request, response, getLoginUrl());
		}

	}
}
