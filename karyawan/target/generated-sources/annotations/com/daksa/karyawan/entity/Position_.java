package com.daksa.karyawan.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Position.class)
public abstract class Position_ {

	public static volatile SingularAttribute<Position, BigDecimal> basicSalary;
	public static volatile SingularAttribute<Position, String> name;
	public static volatile SingularAttribute<Position, String> id;

}

