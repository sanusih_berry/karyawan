package com.daksa.karyawan.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ability.class)
public abstract class Ability_ {

	public static volatile SingularAttribute<Ability, String> subMenu;
	public static volatile SingularAttribute<Ability, String> name;
	public static volatile SingularAttribute<Ability, String> id;
	public static volatile SingularAttribute<Ability, String> menu;

}

